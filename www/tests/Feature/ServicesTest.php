<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ServicesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Get services.
     *
     * @return void
     */
    public function test_get_services()
    {
        $response = $this->get('/api/services');
        $response->assertStatus(401);
    }

    /**
     * Add service.
     *
     * @return void
     */
    public function test_add_service()
    {
        $response = $this->post('/api/services/add', [
            'name' =>  'Test service',
            'product_id' => 'test_uuid'
        ]);
        $response->assertStatus(401);
    }

    /**
     * Edit service.
     *
     * @return void
     */
    public function test_edit_service()
    {
        $response = $this->put('/api/services/edit/1', [
            'name' =>  'Test service'
        ]);
        $response->assertStatus(401);
    }

    /**
     * Delete service.
     *
     * @return void
     */
    public function test_delete_service()
    {
        $response = $this->delete('/api/services/delete/1');
        $response->assertStatus(401);
    }

    /**
     * Upgrade service.
     *
     * @return void
     */
    public function test_upgrade_service()
    {
        $response = $this->post('/api/services/upgrade/1/uuid');
        $response->assertStatus(401);
    }

    /**
     * Downgrade service.
     *
     * @return void
     */
    public function test_downgrade_service()
    {
        $response = $this->post('/api/services/downgrade/1/uuid');
        $response->assertStatus(401);
    }
}
