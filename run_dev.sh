#!/bin/bash 

ENV="$(pwd)/www/.env"

if [[ ! -f "$ENV" ]]; then
    echo "$ENV"
    cp "$ENV.example" "$ENV"
fi

docker-compose up -d