<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['id' => 1], [
            'name' => 'Demo Demo',
            'email' => 'demo@test.com',
            'password' => Hash::make('demo')
        ]);

        User::updateOrCreate(['id' => 2], [
            'name' => 'Demo2 Demo',
            'email' => 'demo2@test.com',
            'password' => Hash::make('demo2')
        ]);
    }
}
