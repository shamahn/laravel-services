<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\Service;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ServicesWithAuthTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * SetUp method.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create();
        Auth::login($user, true);
    }

    /**
     * Get services.
     *
     * @return void
     */
    public function test_get_services()
    {
        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->get('/api/services');
        $response->assertStatus(200);
    }

    /**
     * Add service.
     *
     * @return void
     */
    public function test_add_service()
    {
        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->post('/api/services/add', [
                'name' =>  'Test service',
                'product_id' => 'invalid_uuid'
            ]);
        $response->assertStatus(400);

        $valid_product = Product::first();

        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->post('/api/services/add', [
                'name' =>  'Test service',
                'product_id' => $valid_product->id,
            ]);

        $response->assertStatus(200);
    }

    /**
     * Edit service.
     *
     * @return void
     */
    public function test_edit_service()
    {
        $valid_product = Product::first();
        $this->assertNotEmpty($valid_product);

        $test_service = Service::factory()->create([
            'name' => 'test',
            'product_id' => $valid_product->id,
        ]);
        $this->assertNotEmpty($test_service);

        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->put('/api/services/edit/' . $test_service->id, [
                'name' =>  'Test service new name',
            ]);
        $response->assertStatus(200);
    }

    /**
     * Delete service.
     *
     * @return void
     */
    public function test_delete_service()
    {
        $valid_product = Product::first();
        $this->assertNotEmpty($valid_product);

        $test_service = Service::factory()->create([
            'name' => 'test',
            'product_id' => $valid_product->id,
        ]);
        $this->assertNotEmpty($test_service);

        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->delete('/api/services/delete/' . $test_service->id);
        $response->assertStatus(200);
    }

    /**
     * Upgrade service.
     *
     * @return void
     */
    public function test_upgrade_service()
    {
        $from_product = Product::find('69c89c3d-7564-4c44-97de-b42f6f2ff85b');
        $this->assertNotEmpty($from_product);

        $test_service = Service::factory()->create([
            'name' => 'test',
            'product_id' => $from_product->id,
        ]);
        $this->assertNotEmpty($test_service);

        $to_product = Product::find('e27acb97-afa6-4f18-9c12-04694399edc6');
        $this->assertNotEmpty($to_product);

        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->post('/api/services/upgrade/' . $test_service->id . '/' . $to_product->id);
        $response->assertStatus(200);
    }

    /**
     * Downgrade service.
     *
     * @return void
     */
    public function test_downgrade_service()
    {
        $from_product = Product::find('e27acb97-afa6-4f18-9c12-04694399edc6');
        $this->assertNotEmpty($from_product);

        $test_service = Service::factory()->create([
            'name' => 'test',
            'product_id' => $from_product->id,
        ]);
        $this->assertNotEmpty($test_service);

        $to_product = Product::find('69c89c3d-7564-4c44-97de-b42f6f2ff85b');
        $this->assertNotEmpty($to_product);

        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->post('/api/services/downgrade/' . $test_service->id . '/' . $to_product->id);
        $response->assertStatus(400);

        $to_product = Product::find('e559b5c8-70e4-4e29-9136-f7448440b046');
        $this->assertNotEmpty($to_product);

        $response = $this->actingAs(Auth::user(), 'api')
            ->withSession(['banned' => false])
            ->post('/api/services/downgrade/' . $test_service->id . '/' . $to_product->id);
        $response->assertStatus(200);
    }
}
