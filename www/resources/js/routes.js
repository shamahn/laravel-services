import AllServices from './components/AllServices.vue';
import CreateService from './components/CreateService.vue';
import EditService from './components/EditService.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllServices
    },
    {
        name: 'add',
        path: '/add',
        component: CreateService
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditService
    }
];
