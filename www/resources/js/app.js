/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
import Vue from "vue";

import App from "./App.vue";
import VueAxios from "vue-axios";
import VueRouter from "vue-router";
import axios from "axios";
import { routes } from "./routes";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

axios.defaults.headers.common["Authorization"] = window.secret;
Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const router = new VueRouter({
    base: "/app/",
    mode: "history",
    routes: routes
});

Vue.directive("click-outside", {
    bind() {
        this.event = event => this.vm.$emit(this.expression, event);
        this.el.addEventListener("click", this.stopProp);
        document.body.addEventListener("click", this.event);
    },
    unbind() {
        this.el.removeEventListener("click", this.stopProp);
        document.body.removeEventListener("click", this.event);
    },

    stopProp(event) {
        event.stopPropagation();
    }
});

const app = new Vue({
    el: "#app",
    router: router,
    render: h => h(App)
});
