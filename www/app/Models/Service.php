<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Service extends Model
{
    use HasFactory;

    protected $guard = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'product_id'
    ];

    protected $hidden = ['user_id', 'product_id'];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($service) {
            if (empty($service->user_id)) {
                $service->user_id = Auth::id();
            }
        });
    }


    /**
     * Get the product associated with the service.
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

}
