<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The DiskType enum.
 *
 * @method static self hdd()
 * @method static self ssd()
 */
class DiskType extends Enum
{
    const HDD = 'hdd';
    const SSD = 'ssd';
}
