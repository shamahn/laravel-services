<?php

use App\Enums\DiskType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary()->autoincrement();
            $table->string('name', 20);
            $table->tinyInteger('cpu');
            $table->mediumInteger('memory');
            $table->mediumInteger('disk');
            $table->enum('disk_type', DiskType::keys())->default(DiskType::SSD);
            $table->tinyInteger('position')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
