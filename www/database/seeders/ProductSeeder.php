<?php

namespace Database\Seeders;

use App\Enums\DiskType;
use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    static private $initProducts = [
        [
            'id' => '69c89c3d-7564-4c44-97de-b42f6f2ff85b',
            'name' => 'Тариф 512',
            'cpu' => 1,
            'memory' => 512,
            'disk' => 20,
            'disk_type' => DiskType::SSD,
            'position' => 1
        ],
        [
            'id' => 'e27acb97-afa6-4f18-9c12-04694399edc6',
            'name' => 'Тариф 1024',
            'cpu' => 2,
            'memory' => 1024,
            'disk' => 40,
            'disk_type' => DiskType::SSD,
            'position' => 2
        ],
        [
            'id' => '092cd3c6-92ed-4939-9ebb-657e160af294',
            'name' => 'Тариф 2048',
            'cpu' => 4,
            'memory' => 2048,
            'disk' => 80,
            'disk_type' => DiskType::SSD,
            'position' => 3
        ],
        [
            'id' => '3930866c-1322-4261-8bf8-762be6eff158',
            'name' => 'Тариф 8192',
            'cpu' => 6,
            'memory' => 8192,
            'disk' => 120,
            'disk_type' => DiskType::SSD,
            'position' => 4
        ],
        [
            'id' => 'e559b5c8-70e4-4e29-9136-f7448440b046',
            'name' => 'Тариф D1',
            'cpu' => 1,
            'memory' => 512,
            'disk' => 40,
            'disk_type' => DiskType::SSD,
            'position' => 1
        ],
        [
            'id' => '0e3164f7-8628-4a7a-976a-32bc1428c031',
            'name' => 'Тариф D2',
            'cpu' => 2,
            'memory' => 1024,
            'disk' => 80,
            'disk_type' => DiskType::SSD,
            'position' => 2
        ],
        [
            'id' => '17f82710-6b69-4ea2-9fed-ed3f618f4dc0',
            'name' => 'Тариф D3',
            'cpu' => 4,
            'memory' => 2048,
            'disk' => 120,
            'disk_type' => DiskType::SSD,
            'position' => 3
        ],
        [
            'id' => 'a5b7d523-71be-4b1a-a86f-529dee888624',
            'name' => 'Тариф D4',
            'cpu' => 6,
            'memory' => 8192,
            'disk' => 240,
            'disk_type' => DiskType::SSD,
            'position' => 4
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::$initProducts as $product) {
            Product::updateOrCreate($product);
        }
    }
}
