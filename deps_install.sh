#!/bin/bash

docker-compose exec app composer install
docker-compose exec app npm install
docker-compose exec app npm run prod
docker-compose exec app php artisan passport:install