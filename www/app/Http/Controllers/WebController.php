<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{
    public function index()
    {
        $token = "Bearer " . Auth::user()->createToken('AppName')->accessToken;
        return view('dashboard', [
            'token' => $token,
        ]);
    }
}
