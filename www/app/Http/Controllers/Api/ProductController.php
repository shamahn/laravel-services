<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Service;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function __construct($guard = null)
    {
        Auth::shouldUse('api');
    }

    public function list()
    {
        $services = Product::all();
        return response()->json([
            'success' => true,
            'data' => $services
        ]);
    }
}
