FROM php:7.4-fpm-alpine

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apk upgrade --no-self-upgrade --available && apk add --no-cache \
    git \
    curl \
    libpng-dev \
    oniguruma-dev \
    libxml2-dev \
    zip \
    unzip \
    npm

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN adduser -S -G www-data -u $uid -h /home/$user -D $user && addgroup $user root
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:www-data /home/$user

# Set working directory
WORKDIR /var/www

USER $user