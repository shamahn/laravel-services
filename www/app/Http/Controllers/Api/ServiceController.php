<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Service;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function __construct($guard = null)
    {
        Auth::shouldUse('api');
    }

    public function list()
    {
        $services = Service::with('product')->where('user_id', Auth::id())->get();
        return response()->json([
            'success' => true,
            'data' => $services
        ]);
    }

    /**
     * Add the user service to storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'product_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $product = Product::find($input['product_id']);

        if (!$product) {
            return $this->sendError('DB Error', 'Tariff not found.');
        }

        try {
            $service = Service::create($input);
        } catch (QueryException $exception) {
            return $this->sendError('DB Error', $exception->errorInfo);
        }
        return $this->sendSuccess('Service was created successfully', $service);
    }

    /**
     * Edit user service.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $service = Service::find($id);

        if (!$service) {
            return $this->sendError('App Error', 'Service doesn`t exist');
        }

        try {
            $service->name = $input['name'];
            $service->save();
        } catch (QueryException $exception) {
            return $this->sendError('DB Error', $exception->errorInfo);
        }

        return $this->sendSuccess('Service was changed successfully', $service);
    }

    /**
     * Delete user service.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $service = Service::find($id);

        if (!$service) {
            return $this->sendError('App Error', 'Service doesn`t exist');
        }

        try {
            $service->delete();
        } catch (QueryException $exception) {
            return $this->sendError('DB Error', $exception->errorInfo);
        }

        return $this->sendSuccess('Service was removed successfully', $service);
    }

    /**
     * Get user service by id.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        $service = Service::find($id);

        if (!$service) {
            return $this->sendError('App Error', 'Service doesn`t exist');
        }

        return $this->sendSuccess('', $service);
    }

    /**
     * Upgrade user service.
     *
     * @param  Integer  $id
     * @param  UUID  $product_id
     * @return \Illuminate\Http\Response
     */
    public function upgrade($id, $product_id)
    {
        $service = Service::find($id);

        if (!$service) {
            return $this->sendError('App Error', 'Service doesn`t exist');
        }

        $next_product = Product::find($product_id);
        if (!$next_product) {
            return $this->sendError('App Error', 'Tariff doesn`t exist');
        }

        if ($service->product->position >= $next_product->position) {
            return $this->sendError('App Error', 'Can not upgrade to this tariff');
        }

        try {
            $service->product_id = $next_product->id;
            $service->save();
            $service = Service::find($id);
        } catch (QueryException $exception) {
            return $this->sendError('DB Error', $exception->errorInfo);
        }

        return $this->sendSuccess('Service was upgraded successfully', $service);
    }

    /**
     * Downgrade user service.
     *
     * @param  Integer  $id
     * @param  UUID  $product_id
     * @return \Illuminate\Http\Response
     */
    public function downgrade($id, $product_id)
    {
        $service = Service::find($id);

        if (!$service) {
            return $this->sendError('App Error', 'Service doesn`t exist');
        }

        $next_product = Product::find($product_id);
        if (!$next_product) {
            return $this->sendError('App Error', 'Tariff doesn`t exist');
        }

        if ($service->product->position <= $next_product->position) {
            return $this->sendError('App Error', 'Can not downgrade to this tariff');
        }

        if ($service->product->disk != $next_product->disk) {
            return $this->sendError('App Error', 'Unable to downgrade in automatic mode');
        }

        try {
            $service->product_id = $next_product->id;
            $service->save();
            $service = Service::find($id);
        } catch (QueryException $exception) {
            return $this->sendError('DB Error', $exception->errorInfo);
        }

        return $this->sendSuccess('Service was upgraded successfully', $service);
    }

    private function sendError($type, $data)
    {
        return response()->json([
            'success' => false,
            'errorType' => $type,
            'error' => $data
        ], 400);
    }

    private function sendSuccess($msg, $data)
    {
        return response()->json([
            'success' => true,
            'message' => $msg,
            'data' => $data
        ], 200);
    }
}
