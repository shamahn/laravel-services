<?php

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ServiceController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//get token
Route::post('token', [AuthController::class, 'auth']);

Route::prefix('services')
    ->middleware('api:api')
    ->group(function () {
        Route::get('/', [ServiceController::class, 'list']);
        Route::post('/add', [ServiceController::class, 'add']);
        Route::put('/edit/{id}', [ServiceController::class, 'edit']);
        Route::delete('/delete/{id}', [ServiceController::class, 'delete']);
        Route::get('/{id}', [ServiceController::class, 'get']);
        Route::post('/upgrade/{id}/{product_id}', [ServiceController::class, 'upgrade']);
        Route::post('/downgrade/{id}/{product_id}', [ServiceController::class, 'downgrade']);
    });

Route::prefix('tariffs')
    ->middleware('api:api')
    ->group(function () {
        Route::get('/', [ProductController::class, 'list']);
    });
