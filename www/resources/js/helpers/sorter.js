export const sortTariffs = tariffs => {
    return tariffs.sort((a, b) => {
        let aName = a.name.replace("Тариф", "").replace(" ", "");
        let bName = b.name.replace("Тариф", "").replace(" ", "");
        a = Number(aName);
        b = Number(bName);

        if (!a && b) {
            return 1;
        }
        if (a && !b) {
            return -1;
        }
        if (!a) {
            a = aName;
        }
        if (!b) {
            b = bName;
        }
        return a > b ? 1 : -1;
    });
};
