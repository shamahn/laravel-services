<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuids;

    protected $perPage = 100;

    protected $fillable = ['name', 'cpu', 'memory', 'disk', 'disk_type'];

    protected $hidden = ['created_at', 'updated_at'];
}
